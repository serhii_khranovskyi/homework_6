package com.epam.rd.khranovskyi.model;

import com.epam.rd.khranovskyi.annotation.Bench;
import org.springframework.stereotype.Component;

@Component
public class JavaDeveloper implements Developer {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    @Bench
    public void writeCode() {
        System.out.println("Java developer writes Java code...");
    }

    @Override
    @Bench
    public void refactorCode() {
        System.out.println("Java developer refactor Java code...");
        getAnError();
    }

    @Override
    public void getAnError() {
        System.out.println("Java developer got an error");
        throw new NullPointerException();
    }
}
