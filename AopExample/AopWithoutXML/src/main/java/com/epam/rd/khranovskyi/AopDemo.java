package com.epam.rd.khranovskyi;

import com.epam.rd.khranovskyi.model.Developer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class AopDemo {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext("com.epam.rd.khranovskyi");
        Developer developer = (Developer) context.getBean("javaDeveloper");
        developer.writeCode();
        developer.refactorCode();
    }
}
