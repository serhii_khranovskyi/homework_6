package com.epam.rd.khranovskyi.model;

public interface Developer {
   void writeCode();
   void refactorCode();
   void getAnError();
}
