package com.epam.rd.khranovskyi.model;

public class JavaDeveloper implements Developer {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void writeCode() {
        System.out.println("Java developer writes Java code...");
    }

    @Override
    public void refactorCode() {
        System.out.println("Java developer refactor Java code...");
        getAnError();
    }

    @Override
    public void getAnError() {
        System.out.println("Java developer got an error");
        throw new NullPointerException();
    }
}
