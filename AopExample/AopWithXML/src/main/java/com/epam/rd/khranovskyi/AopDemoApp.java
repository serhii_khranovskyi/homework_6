package com.epam.rd.khranovskyi;

import com.epam.rd.khranovskyi.model.Developer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AopDemoApp {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        Developer developer = (Developer) context.getBean("developer");
        developer.writeCode();
        developer.refactorCode();
    }
}
