package com.epam.rd.khranovskyi.advice;

import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.util.StopWatch;

public class Advice {

    public Object around(ProceedingJoinPoint joinPoint) {
        try {
            String declaringTypeName = joinPoint.getSignature().getDeclaringTypeName();
            String name = joinPoint.getSignature().getName();
            Object[] args = joinPoint.getArgs();
            System.out.println("~~~~BEFORE~~~~" + System.lineSeparator());
            System.out.println("Object type name--> " + declaringTypeName);
            System.out.println("Method--> " + name);
            System.out.println("With arguments: ");
            if (args.length != 0) {
                for (Object arg : args) {
                    System.out.println("    -> " + arg);
                }
            } else {
                System.out.println("    -> Method has no arguments!");
            }
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            System.out.println("~~~~~METHOD INVOKE~~~~" + System.lineSeparator());
            Object returnValue = joinPoint.proceed();
            stopWatch.stop();
            System.out.println(System.lineSeparator() + "~~~~~METHOD ENDED~~~~~~");
            System.out.println("Execution took -->" + stopWatch.getLastTaskTimeMillis());
            return returnValue;
        } catch (Throwable e) {
            System.out.println("~~~~~METHOD THROWS EXCEPTION~~~~~~" + System.lineSeparator());
            throw new IllegalArgumentException("Method "
                    + joinPoint.getSignature().getName()
                    + " caused an Exception!", e);
        }
    }
}
